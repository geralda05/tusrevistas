# Gestor de Revistas TuRevista.com

Gestor de contenido realizado con fines educacionales dentro de la Universidad Valle del Momboy

## Autores del Proyecto

Programación: Gerald Leonel Alarcón
Metodología y Desarrollo: Angelica Balza, Benny Hurtado, David Gonzalez

## Caracteristicas del proyecto

- El sistema está hecho en Node.js. Globalmente el sistema sólo gestiona revistas basadas en una base de datos MYSQL (/recursos), que puede contener uno o más artículos dentro de su ID.

- Tiene un sistema de autenticación basado en Google Firebase relacionado con Vue.js

- Los usuarios solo tienen acceso a la página principal donde solo pueden ver las revistas y sus artículos. Solo los administradores pueden agregar, modificar y eliminar revistas.

- La parte pública de la página tiene una página de destino donde los usuarios pueden suscribirse. En el panel de administración puede ver todos los suscritos y borrarlos.

## Ejecutar el proyecto en modo desarrollo

Para ejecutar el proyecto, debe descargar el repositorio y tener el servicio MySQL activo. La base de datos debe llamarse "taller6_proyecto" y debe importar el archivo seed.sql que está dentro de la carpeta /recursos.

Posteriormente, se deben instalar las dependencias que están dentro de las carpetas /turevista y /webpack. Una vez hecho el ```npm install``` dentro de cada uno de ellos, se pone en modo de desarrollo con los comandos ```npm start``` en la carpeta /turevista y ```npm run webpack: watch``` en la carpeta /webpack. 

Puede visualizarse correctamente en el puerto 3000.

# ENGLISH 

# Magazines Manager TuRevista.com

Content manager made for educational purposes within Universidad Valle del Momboy 

## Project Authors

Programming: Gerald Leonel Alarcón
Methodology and Development: Angelica Balza, Benny Hurtado, David Gonzalez

## Project features

- The system is made in Node.js. Globally, the system only manages journals based on a MYSQL database (/resources), which may contain one or more articles within its ID.

- It has an authentication system based on Google Firebase related to Vue.js

- Users only have access to the main page where they can only see the magazines and their articles. Only administrators can add, modify and delete magazines.

- The public part of the page has a landing page where users can subscribe. In the administration panel you can see all the subscribers and delete them.

## Run the project in development mode

To run the project, you must download the repository and have the MySQL service active. The database must be called "taller6_proyecto" and you must import the seed.sql file that is inside the folder /resources.

Subsequently, the dependencies that are inside the /turevista and /webpack folders must be installed: Once the ```npm install``` is done inside each of them, it is put into development mode with the commands ```npm start``` in the folder /turevista and ```npm run webpack: watch ``` in the /webpack folder.

It can be displayed correctly on port 3000.

