var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var collector = express();

collector.use(bodyParser.urlencoded({
  extended:true,
}))

collector.use(bodyParser.json());
collector.use(cookieParser());
collector.use(express.static(path.join(__dirname, 'public')));

/* Conexión de la BD */
var connections = require('../configs/connection');

/* Leer Revistas de la BD */
collector.get('/leer/:RevistaID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.RevistaID ? req.params.RevistaID:null),
        ]
        var SQL = (req.params.RevistaID ? "SELECT * FROM revistas WHERE RevistaID=?":"SELECT * FROM revistas");
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Leer las 3 ultimas noticias de la BD */
collector.get('/leernew/:ArticuloID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.ArticuloID ? req.params.ArticuloID:null),
        ]
        var SQL = (req.params.RevistaID ? "SELECT * FROM articulos WHERE ArticuloID=? ORDER BY ArticuloID DESC limit 3":"SELECT * FROM articulos ORDER BY ArticuloID DESC limit 3");
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.get('/leernew2/:ArticuloID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.ArticuloID ? req.params.ArticuloID:null),
        ]
        var SQL = (req.params.ArticuloID ? "SELECT * FROM articulos WHERE ArticuloID=? ORDER BY ArticuloID ASC limit 3":"SELECT * FROM articulos ORDER BY ArticuloID ASC limit 3");
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Buscar de la BD */
collector.get('/buscararticulos/:RevistaID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            req.body.BusquedaArticulo,
        ]
        var SQL = "SELECT * FROM articulos WHERE * LIKE '%?%'";
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Leer Suscriptores de la BD */
collector.get('/leersuscriptor/:SuscriptorID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.SuscriptorID ? req.params.SuscriptorID:null),
        ]
        var SQL = (req.params.SuscriptorID ? "SELECT * FROM suscriptores WHERE SuscriptorID=?":"SELECT * FROM suscriptores");
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Leer Usuarios de la BD */
collector.get('/leerusuarios/:UsuarioID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.UsuarioID ? req.params.UsuarioID:null),
        ]
        var SQL = (req.params.UsuarioID ? "SELECT * FROM usuarios WHERE UsuarioID=?":"SELECT * FROM usuarios");
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Leer Articulos de una Revista Determinada en la BD */
collector.get('/leer2/:RevistaID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.RevistaID ? req.params.RevistaID:null),
        ]
        var SQL = (req.params.RevistaID ? "SELECT * FROM articulos INNER JOIN revistas WHERE articulos.ArticuloRevistaID=?":"SELECT * FROM articulos");
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Leer Articulos de una Revista Determinada en la BD */
collector.get('/buscar2/:ArticuloID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.ArticuloID ? req.params.ArticuloID:null),
        ]
        var SQL = (req.params.ArticuloID ? "SELECT * FROM articulos INNER JOIN busquedas WHERE articulos.ArticuloAutor1 = busquedas.BusquedaAutor OR articulos.ArticuloAutor2 = busquedas.BusquedaAutor OR articulos.ArticuloAutor3 = busquedas.BusquedaAutor OR articulos.ArticuloAutor4 = busquedas.BusquedaAutor OR articulos.ArticuloAutor5 = busquedas.BusquedaAutor OR articulos.ArticuloAutor6 = busquedas.BusquedaAutor OR articulos.ArticuloAutor7 = busquedas.BusquedaAutor OR articulos.ArticuloAutor8 = busquedas.BusquedaAutor OR articulos.ArticuloAutor9 = busquedas.BusquedaAutor OR articulos.ArticuloCategoria = busquedas.BusquedaCategoria":"SELECT * FROM articulos INNER JOIN busquedas WHERE articulos.ArticuloAutor1 = busquedas.BusquedaAutor OR articulos.ArticuloAutor2 = busquedas.BusquedaAutor OR articulos.ArticuloAutor3 = busquedas.BusquedaAutor OR articulos.ArticuloAutor4 = busquedas.BusquedaAutor OR articulos.ArticuloAutor5 = busquedas.BusquedaAutor OR articulos.ArticuloAutor6 = busquedas.BusquedaAutor OR articulos.ArticuloAutor7 = busquedas.BusquedaAutor OR articulos.ArticuloAutor8 = busquedas.BusquedaAutor OR articulos.ArticuloAutor9 = busquedas.BusquedaAutor OR articulos.ArticuloCategoria = busquedas.BusquedaCategoria");
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})


/* Leer Articulo de la BD */
collector.get('/leer3/:ArticuloID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.ArticuloID ? req.params.ArticuloID:null),
        ]
        var SQL = (req.params.ArticuloID ? "SELECT * FROM articulos WHERE ArticuloID=?":"SELECT * FROM articulos");
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})


/* Leer Registros de la BD */
collector.get('/articles/:ArticuloRevistaID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.ArticuloRevistaID ? req.params.ArticuloRevistaID:null),
        ]
        var SQL = (req.params.ArticuloRevistaID ? "SELECT * FROM articulos WHERE ArticuloRevistaID=?":"SELECT * FROM articulos");
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Leer Registros de la BD para modificar o eliminar */
collector.get('/articles2/:ArticuloID?',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        let Request = [
            (req.params.ArticuloID ? req.params.ArticuloID:null),
        ]
        var SQL = (req.params.ArticuloID ? "SELECT * FROM articulos WHERE ArticuloID=?":"SELECT * FROM articulos");
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Agregar a la BD */
collector.post('/agregar',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.RevistaNombre !== 'undefined'){

        let Request = [
            req.body.RevistaNombre,
            req.body.RevistaCategoria,
        ]
        var SQL = "INSERT INTO revistas (RevistaNombre,RevistaCategoria) VALUES (?,?)";
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

collector.post('/buscar1',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.BusquedaAutor !== 'undefined'){

        let Request = [
            req.body.BusquedaCategoria,
            req.body.BusquedaAutor,
        ]
        var SQL = "INSERT INTO busquedas (BusquedaCategoria,BusquedaAutor) VALUES (?,?)";
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Agregar a la BD */
collector.post('/agregararticulo',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.ArticuloNombre !== 'undefined'){

        let Request = [
            req.body.ArticuloRevistaID,
            req.body.ArticuloNombre,
            req.body.ArticuloCategoria,
            req.body.ArticuloContenido,
            req.body.ArticuloAutor1,
            req.body.ArticuloAutor2,
            req.body.ArticuloAutor3,
            req.body.ArticuloAutor4,
            req.body.ArticuloAutor5,
            req.body.ArticuloAutor6,
            req.body.ArticuloAutor7,
            req.body.ArticuloAutor8,
            req.body.ArticuloAutor9,   
            req.body.ArticuloImagen,                 
        ]
        var SQL = "INSERT INTO articulos (ArticuloRevistaID,ArticuloNombre,ArticuloCategoria,ArticuloContenido,ArticuloAutor1,ArticuloAutor2,ArticuloAutor3,ArticuloAutor4,ArticuloAutor5,ArticuloAutor6,ArticuloAutor7,ArticuloAutor8,ArticuloAutor9,ArticuloImagen) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Agregar a la BD */
collector.post('/agregarsuscriptor',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.SuscriptorNombre !== 'undefined'){

        let Request = [
            req.body.SuscriptorNombre,
            req.body.SuscriptorEmail,
        ]
        var SQL = "INSERT INTO suscriptores (SuscriptorNombre,SuscriptorEmail) VALUES (?,?)";
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Agregar a la BD */
collector.post('/agregarusuario',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.UsuarioNombre !== 'undefined'){

        let Request = [
            req.body.UsuarioNombre,
            req.body.UsuarioUser,
            req.body.UsuarioClave,
        ]
        var SQL = "INSERT INTO usuarios (UsuarioNombre,UsuarioUser,UsuarioClave) VALUES (?,?,MD5(?))";
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Modificar en la BD */
collector.post('/modificar/:RevistaID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.RevistaNombre !== 'undefined'){

        let Request = [
            req.body.RevistaNombre,
            req.body.RevistaCategoria,
        ]
        var SQL = "UPDATE revistas SET RevistaNombre=? , RevistaCategoria=? WHERE RevistaID='"+req.params.RevistaID+"'";
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Modificar en la BD */
collector.post('/modificarusuario/:UsuarioID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.UsuarioNombre !== 'undefined'){

        let Request = [
            req.body.UsuarioNombre,
            req.body.UsuarioUser,
            req.body.UsuarioClave,
        ]
        var SQL = "UPDATE usuarios SET UsuarioNombre=? , UsuarioUser=? , UsuarioClave=MD5(?) WHERE UsuarioID='"+req.params.UsuarioID+"'";
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Modificar en la BD */
collector.post('/modificararticulos/:ArticuloID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        if(typeof req.body.ArticuloNombre !== 'undefined'){

        let Request = [
            req.body.ArticuloNombre,
            req.body.ArticuloCategoria,
            req.body.ArticuloContenido,
            req.body.ArticuloImagen,
            req.body.ArticuloAutor1,
            req.body.ArticuloAutor2,
            req.body.ArticuloAutor3,
            req.body.ArticuloAutor4,
            req.body.ArticuloAutor5,
            req.body.ArticuloAutor6,
            req.body.ArticuloAutor7,
            req.body.ArticuloAutor8,
            req.body.ArticuloAutor9,

        ]
        var SQL = "UPDATE articulos SET ArticuloNombre=? , ArticuloCategoria=? , ArticuloContenido=? , ArticuloImagen=? , ArticuloAutor1=? , ArticuloAutor2=? , ArticuloAutor3=? , ArticuloAutor4=? , ArticuloAutor5=? , ArticuloAutor6=? , ArticuloAutor7=? , ArticuloAutor8=? , ArticuloAutor9=? WHERE ArticuloID='"+req.params.ArticuloID+"'";
        connections.taller6_proyecto.query(SQL,Request,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    }else{
        Response.status=false;
        Response.message="Datos incompletos";
        res.send(Response);
    }
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Eliminar en la BD */
collector.get('/eliminar/:RevistaID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        var SQL = "DELETE FROM revistas WHERe RevistaID='"+req.params.RevistaID+"'";
        connections.taller6_proyecto.query(SQL,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Limpiar Cache en la BD */
collector.get('/eliminarbusquedas',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        var SQL = "TRUNCATE TABLE busquedas";
        connections.taller6_proyecto.query(SQL,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Eliminar en la BD */
collector.get('/eliminarsuscriptor/:SuscriptorID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        var SQL = "DELETE FROM suscriptores WHERe SuscriptorID='"+req.params.SuscriptorID+"'";
        connections.taller6_proyecto.query(SQL,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Eliminar en la BD */
collector.get('/eliminararticulo/:ArticuloID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        var SQL = "DELETE FROM articulos WHERe ArticuloID='"+req.params.ArticuloID+"'";
        connections.taller6_proyecto.query(SQL,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

/* Función de Eliminar Usuario en la BD */
collector.get('/eliminarusuario/:UsuarioID',function(req,res){
    var Response = {
        status:true,
        message:'',
    }
    try{
        var SQL = "DELETE FROM usuarios WHERe UsuarioID='"+req.params.UsuarioID+"'";
        connections.taller6_proyecto.query(SQL,function(error,rows){
            if(error){
                Response.status=false;
                Response.message=String(error);
            }else{
                Response.values=rows;
            }
            res.send(Response);
            return res.end();
        });
    } catch (error) {
        Response.status=false;
        Response.message=String(error);
        res.send(Response);
    }
})

module.exports = collector;