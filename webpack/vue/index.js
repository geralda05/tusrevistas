import Vue from 'vue'
import VueRouter from 'vue-router'
import firebase from 'firebase'

Vue.use(VueRouter);

import index from './views/index.vue'
import inicio from './views/templates/inicio/inicio.vue'
import nosotros from './views/templates/nosotros/nosotros.vue'
import revistas from './views/templates/revistas/revistas.vue'
import rwrevistas from './views/templates/revistas/rw-revistas.vue'
import articulo from './views/templates/revistas/articulo.vue'
import buscar from './views/templates/revistas/rw-buscar.vue'
import buscararticulo from './views/templates/revistas/buscar.vue'
import busqueda from './views/templates/revistas/search.vue'
import contact from './views/templates/contacto/contact.vue'
import vFooter from './views/templates/components/footer.vue'
import adminrevistas from './views/templates/admin/components/revistas.vue'
import suscriptores from './views/templates/admin/components/suscriptores.vue'
import subscribers from './views/templates/admin/components/subscribers.vue'
import usuarios from './views/templates/admin/components/usuarios.vue'
import nuevousuario from './views/templates/admin/components/agregar/nuevousuario.vue'
import admin from './views/templates/admin/admin.vue'
import modificarrevista from './views/templates/admin/components/modificar-revista.vue'
import adminmodificarrevista from './views/templates/admin/components/modificar/modificar-revista.vue'
import adminmodificararticulo from './views/templates/admin/components/modificar/modificar-articulo.vue'
import admineliminarrevista from './views/templates/admin/components/eliminar/eliminar-revista.vue'
import admineliminararticulo from './views/templates/admin/components/eliminar/eliminar-articulo.vue'
import eliminarusuario from './views/templates/admin/components/eliminar/eliminar-usuario.vue'
import modificarusuario from './views/templates/admin/components/modificar/modificar-usuario.vue'
import eliminarsuscriptor from './views/templates/admin/components/eliminar/eliminar-suscriptor.vue'
import adminarticulo from './views/templates/admin/components/articulo.vue'
import adminnueva from './views/templates/admin/components/agregar/nueva-revista.vue'
import adminnuevo from './views/templates/admin/components/agregar/nuevo-articulo.vue'
import users from './views/templates/admin/components/users.vue'
import error404 from './views/templates/errores/404.vue'
  
const router = new VueRouter({
    mode:'history',
    routes:[
        {
            path:'/',
            redirect: "/inicio",

        },
        {
            path:'/inicio',
            name:"inicio",
            component: inicio,
        },
        {
            path:'/revistas',
            name:"rwrevistas",
            component: rwrevistas,
            props:true,
            children:[
                {
                    path:'/',
                    name:"revistas",
                    component: revistas,
                }, 
                {
                    path:':ArticuloID/verarticulo',
                    name:"articulo",
                    component: articulo,
                    props:true,
                },               
                {
                    path:'buscar',
                    name:"buscar",
                    component: buscar,
                    children:[
                        {
                            path:'/',
                            name:"buscararticulo",
                            component: buscararticulo,
                        },      
                        {
                            path:'busqueda',
                            name:"busqueda",
                            component: busqueda,
                        },
                          
                    ]
                },        
            ]
        },
        {
            path:'/contact',
            name:"contact",
            component: contact,
        },
        {
            path:'/nosotros',
            name:"nosotros",
            component: nosotros,
        },
        {   path:'/admin',
            name:"admin",
            component:admin,
            meta:{
                autentificado: true,
            },
            children:[
                {
                    path:'/',
                    name:"adminrevistas",
                    component: adminrevistas,
                },
                {
                    path:'nueva',
                    name:"adminnueva",
                    component: adminnueva,
                },
                {
                    path:':RevistaID/eliminar',
                    name:"admineliminarrevista",
                    component: admineliminarrevista,
                    props:true,
                }, 
                {
                    path:':ArticuloID/articulo',
                    name:"adminarticulo",
                    component: adminarticulo,
                    props:true,
                },
                {
                    path:':RevistaID/modificar',
                    name:"modificarrevista",
                    component: modificarrevista,
                    props:true,
                    children:[
                        {
                            path:'/',
                            name:"adminmodificarrevista",
                            component: adminmodificarrevista,
                            props:true,
                        },
                        {
                            path:':ArticuloID/modificararticulo',
                            name:"adminmodificararticulo",
                            component: adminmodificararticulo,
                            props:true,
                        },
                        {
                            path:':ArticuloID/eliminararticulo',
                            name:"admineliminararticulo",
                            component: admineliminararticulo,
                            props:true,
                        },  
                        {
                            path:'nuevoarticulo',
                            name:"adminnuevo",
                            component: adminnuevo,
                            props:true,
                        },                      
                    ]
                },  
                {
                    path:'subscribers',
                    name:"subscribers",
                    component: subscribers,
                    props:true,
                    children:[
                        {
                            path:'/',
                            name:'suscriptores',
                            component: suscriptores,
                            props:true,
                        },
                        {
                            path:':SuscriptorID/eliminar',
                            name:'eliminarsuscriptor',
                            component: eliminarsuscriptor,
                            props:true,
                        }
                    ]
                },
                {
                    path:'users',
                    name:"users",
                    component: users,
                    props:true,
                    children:[
                        {
                            path:'/',
                            name:"usuarios",
                            component: usuarios,
                        },
                        {
                            path:'nuevousuario',
                            name:"nuevousuario",
                            component: nuevousuario,
                        },
                        {
                            path:':UsuarioID/eliminar',
                            name:"eliminarusuario",
                            component: eliminarusuario,
                            props:true,
                        },
                        {
                            path:':UsuarioID/modificar',
                            name:"modificarusuario",
                            component: modificarusuario,
                            props:true,
                        },
                    ]
                },                
            ]
        },
        {
            path:'/404',
            name:"error404",
            component: error404,
        },
        {
            path: '*',
            redirect:"/404",
        }
    ]
});
    // Initialize Firebase
    var config = {
        apiKey: "AIzaSyCC8W9khGq7JMrgsODftXXt09DDgsujzwA",
        authDomain: "taller6-proyecto.firebaseapp.com",
        databaseURL: "https://taller6-proyecto.firebaseio.com",
        projectId: "taller6-proyecto",
        storageBucket: "taller6-proyecto.appspot.com",
        messagingSenderId: "858992140415"
    };

    firebase.initializeApp(config);

    firebase.auth().onAuthStateChanged(function(user){
        const app = new Vue({
            router,
            render: createEle => createEle(index)
        
        }).$mount('#app');
    });

    router.beforeEach((to, from, next) => {

        let usuario = firebase.auth().currentUser;
        console.log(usuario);
        let autorizacion = to.matched.some(record => record.meta.autentificado);

        if (autorizacion && !usuario) {
            next('inicio')
        } else if (!autorizacion && usuario){
            next('admin');
        } else {
            next();
        }
    })

    export default router;

