-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-12-2018 a las 10:04:43
-- Versión del servidor: 10.1.31-MariaDB
-- Versión de PHP: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `taller6_proyecto`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `articulos`
--

CREATE TABLE `articulos` (
  `ArticuloID` int(11) NOT NULL,
  `ArticuloRevistaID` int(11) NOT NULL,
  `ArticuloNombre` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloCategoria` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloContenido` varchar(10000) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloAutor1` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloAutor2` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloAutor3` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloAutor4` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloAutor5` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloAutor6` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloAutor7` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloAutor8` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloAutor9` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ArticuloImagen` varchar(600) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `busquedas`
--

CREATE TABLE `busquedas` (
  `BusquedaID` int(11) NOT NULL,
  `BusquedaCategoria` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `BusquedaAutor` varchar(600) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `revistas`
--

CREATE TABLE `revistas` (
  `RevistaID` int(11) NOT NULL,
  `RevistaNombre` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `RevistaCategoria` varchar(600) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `suscriptores`
--

CREATE TABLE `suscriptores` (
  `SuscriptorID` int(11) NOT NULL,
  `SuscriptorNombre` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `SuscriptorEmail` varchar(240) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `UsuarioID` int(11) NOT NULL,
  `UsuarioNombre` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `UsuarioUser` varchar(600) COLLATE utf8_spanish_ci NOT NULL,
  `UsuarioClave` varchar(600) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `articulos`
--
ALTER TABLE `articulos`
  ADD PRIMARY KEY (`ArticuloID`);

--
-- Indices de la tabla `busquedas`
--
ALTER TABLE `busquedas`
  ADD PRIMARY KEY (`BusquedaID`);

--
-- Indices de la tabla `revistas`
--
ALTER TABLE `revistas`
  ADD PRIMARY KEY (`RevistaID`);

--
-- Indices de la tabla `suscriptores`
--
ALTER TABLE `suscriptores`
  ADD PRIMARY KEY (`SuscriptorID`),
  ADD UNIQUE KEY `SuscriptorEmail` (`SuscriptorEmail`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`UsuarioID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `articulos`
--
ALTER TABLE `articulos`
  MODIFY `ArticuloID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `busquedas`
--
ALTER TABLE `busquedas`
  MODIFY `BusquedaID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `revistas`
--
ALTER TABLE `revistas`
  MODIFY `RevistaID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `suscriptores`
--
ALTER TABLE `suscriptores`
  MODIFY `SuscriptorID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `UsuarioID` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
